package permissions;

import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import static org.mockito.Mockito.*;

import permissions.db.PersonDbManager;
import permissions.db.AddressDbManager;
import permissions.db.PersonRepository;
import permissions.domain.Person;
import permissions.domain.Address;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }

    public void testClearPersons(){
        try{
            PersonDbManager personsDB = new PersonDbManager();
            personsDB.add( new Person() );
            personsDB.clear();
            assert( personsDB.getAll().size() == 0 );
        } catch( SQLException exception ){
            exception.printStackTrace();
        }
    }

    public void testAddPersons(){
        try{
            PersonDbManager personsDB = new PersonDbManager();
            personsDB.clear();
            personsDB.add( new Person().withName( "friend" ) );
            
            List<Person> persons = personsDB.getAll();
            assert( persons.size() == 1 );
            assert( persons.get(0).getName().equals( "friend" ) );
        }catch( SQLException exception ){
            exception.printStackTrace();
        }
    }

    public void testPersonRepo(){
        try{
            PersonDbManager personDB = mock( PersonDbManager.class );
            AddressDbManager addressDB = mock( AddressDbManager.class );

            Address address = new Address()
                .setCountry( "Poland" )
                .setCity("Kraków");
            List testList = new ArrayList();
            Person testPerson = new Person().withName( "friend" ).setId(5);
            testList.add( testPerson.duplicate() );
            when( personDB.getAll() ).thenReturn( testList );

            testPerson.addAddress( address );
            List<Address> addressList = new ArrayList();
            addressList.add( address );
            when( addressDB.getAll() ).thenReturn( addressList );

            PersonRepository persons = new PersonRepository( personDB, addressDB );
            assert( persons.all().size() == 1 );
            assert( persons.all().get(0).getName().equals( "friend" ) );

            Person retPerson = persons.all().get(0);
            assert( retPerson.getAddresses().size() == 1 );
            assert( retPerson.getAddresses().get(0).getCity().equals( "Kraków" ) );

            verify( personDB, times(1) ).getAll();
            verify( addressDB, times(1) ).getAll();
        }catch( SQLException exception ){
            exception.printStackTrace();
        }
    }

    public void testPersonAddressesPersistence(){
        try{
            Address a1 = new Address().setCountry( "Poland" )
                                      .setCity( "Gdańsk" );
            Address a2 = new Address().setCountry( "United Kingdom" )
                                      .setCity( "London" );

            AddressDbManager sut = new AddressDbManager();
            sut.clear();
            assert( sut.getAll().size() == 0 );
            sut.add( a1 );
            sut.add( a2 );
            List<Address> all = sut.getAll();

            assert( all.size() == 2 );
            assert( all.contains( a1 ) );
            assert( all.contains( a2 ) );
        }catch( SQLException exception ){
            exception.printStackTrace();
        }
    }
}
