#!/bin/sh

if [[ $# != 1 ]]; then
    echo "give me source class name" >&2
    exit 1
fi

class_name=$1
this_class="$1""DbManager"

echo "package permissions.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import permissions.domain.$class_name;

public class $this_class extends DbManager{
    public $this_class () throws SQLException{
        super();
    }

    protected void tableInDb() throws SQLException{
        if( !doesTableExists( \"$class_name\" ) ){
            connection.createStatement().executeUpdate(
                // PUT SQL HERE
            );
        }
    }

    protected void preparedStatements() throws SQLException{
        insert = connection.prepareStatement(
            \"INSERT INTO person(name,surname) VALUES(?,?)\"
        );
        select = connection.prepareStatement(
            \"SELECT * FROM $class_name\"
        );
        delete = connection.prepareStatement(
            \"DELETE FROM $class_name WHERE id=?\"
        );
        deleteAll = connection.prepareStatement(
            \"DELETE FROM $class_name\"
        );
        update = connection.prepareStatement(
            \"UPDATE person SET (name,surname)=(?,?) WHERE id=?\"
        );
    }
	
	public void add(Person person) throws SQLException{
        insert.setString(1, person.getName());
        insert.setString(2, person.getSurname());

        insert.executeUpdate();
	}
	
	public void deleteById(int id) throws SQLException{
        delete.setInt(1, id);
        delete.executeUpdate();
	}

    public void clear() throws SQLException{
        deleteAll.executeUpdate();
    }
	
	public void update(Person p) throws SQLException{
        update.setString(1, p.getName());
        update.setString(2, p.getSurname());
        update.setInt(3, p.getId());
        update.executeUpdate();
	}
	
	public List<Person> getAll() throws SQLException{
		List<Person> result = new ArrayList<Person>();
		
        ResultSet rs = select.executeQuery();
        while(rs.next()){
            Person person = new Person();
            person.setName(rs.getString("name"));
            person.setSurname(rs.getString("surname"));
            person.setId(rs.getInt("id"));
            result.add(person);
        }
		return result;
	}

	private PreparedStatement insert;
	private PreparedStatement select;
	private PreparedStatement delete;
    private PreparedStatement deleteAll;
	private PreparedStatement update;
}
" > $this_class.java

