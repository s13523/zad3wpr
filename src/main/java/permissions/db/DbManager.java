package permissions.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class DbManager{
    public DbManager() throws SQLException{
        connection = DriverManager.getConnection(url);

        tableInDb();
        preparedStatements();
    }

    protected abstract void tableInDb() throws SQLException;
    protected abstract void preparedStatements() throws SQLException;

    protected boolean doesTableExists( String name ) throws SQLException{
        boolean tableExists =false;

        ResultSet rs = connection.getMetaData().getTables(
            null, null, null, null
        );

        while(rs.next())
        {
            if(rs.getString("TABLE_NAME").equalsIgnoreCase(name)){
                tableExists=true;
                break;
            }
        }
			
        return tableExists;
    }

	protected Connection connection;
	private String url = "jdbc:hsqldb:hsql://localhost/workdb";
}
