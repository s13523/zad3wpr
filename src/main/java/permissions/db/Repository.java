package permissions.db;

import java.sql.SQLException;
import java.util.List;

public interface Repository<TEntity> {
	public List<TEntity> all() throws SQLException;
	public void add(TEntity entity) throws SQLException;
	public void modify(TEntity entity) throws SQLException;
	public void remove(TEntity entity) throws SQLException;
}
