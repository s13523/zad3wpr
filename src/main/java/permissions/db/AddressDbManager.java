package permissions.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import permissions.domain.Address;

public class AddressDbManager extends DbManager{
    public AddressDbManager () throws SQLException{
        super();
    }

    protected void tableInDb() throws SQLException{
        if( !doesTableExists( "address" ) ){
            connection.createStatement().executeUpdate(
                "CREATE TABLE Address( "
                + "country varchar(40),"
                + "city varchar(40),"
                + "street varchar(40),"
                + "postal varchar(10),"
                + "houseno varchar(5),"
                + "locno varchar(5)"
                + ")"
            );
        }
    }

    protected void preparedStatements() throws SQLException{
        insert = connection.prepareStatement(
            "INSERT INTO address"
            +"(country,city,street,postal,houseno,locno)"
            +"VALUES(?,?,?,?,?,?)"
        );
        select = connection.prepareStatement(
            "SELECT * FROM Address"
        );
        deleteAll = connection.prepareStatement(
            "DELETE FROM Address"
        );
    }
	
	public void add(Address address) throws SQLException{
        insert.setString(1, address.getCountry());
        insert.setString(2, address.getCity());
        insert.setString(3, address.getStreet());
        insert.setString(4, address.getPostalCode());
        insert.setString(5, address.getHouseNumber());
        insert.setString(6, address.getLocalNumber());

        insert.executeUpdate();
	}
	
    public void clear() throws SQLException{
        deleteAll.executeUpdate();
    }
	
	public List<Address> getAll() throws SQLException{
		List<Address> result = new ArrayList<Address>();
		
        ResultSet rs = select.executeQuery();
        while(rs.next()){
            Address address = new Address();
            address.setCountry( rs.getString( "country" ) );
            address.setCity( rs.getString( "city" ) );
            address.setStreet( rs.getString( "street" ) );
            address.setPostalCode( rs.getString( "postal" ) );
            address.setHouseNumber( rs.getString( "houseno" ) );
            address.setLocalNumber( rs.getString( "locno" ) );
            result.add(address);
        }
		return result;
	}

	private PreparedStatement insert;
	private PreparedStatement select;
    private PreparedStatement deleteAll;
}

