package permissions.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import permissions.domain.Address;
import permissions.domain.Person;

public class PersonRepository implements Repository<Person>{
    public PersonRepository(
        PersonDbManager personDbm,
        AddressDbManager addressDbm
    ){
        this.personDbm = personDbm;
        this.addressDbm = addressDbm;
    }

	public List<Person> all() throws SQLException{
        if( personsCache == null )
            personsCache = personDbm.getAll();

        if( addressCache == null ){
            addressCache = addressDbm.getAll();

            for( Address a : addressCache ){
                for( Person p: personsCache ){
                    if( a.getPersonId() == p.getId() )
                        p.addAddress( a );
                }
            }
        }

        return personsCache;
    }

	public void add(Person entity) throws SQLException{
        personDbm.add( entity );
    }

	public void modify(Person entity) throws SQLException{
        personDbm.update( entity );
    }

	public void remove(Person entity) throws SQLException{
        personDbm.deleteById( entity.getId() );
    }

    PersonDbManager personDbm;
    AddressDbManager addressDbm;
    List<Person> personsCache = null;
    List<Address> addressCache = null;
}

