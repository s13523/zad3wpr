package permissions.domain;

import java.util.List;
import java.util.ArrayList;

public class Person {

	private int id;
	private String name;
	private String surname;
	private User user;
	
	private List<Address> addresses = new ArrayList<Address>();

	public int getId() {
		return id;
	}

	public Person setId(int id) {
		this.id = id;
        return this;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public Person withName(String name){
        setName(name);
        return this;
    }

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}
	
    public Person addAddress( Address address ){
        addresses.add( address );
        address.setPersonId( id );
        return this;
    }

    public Person duplicate(){
        Person dup = new Person();
        dup.setId( getId() );
        dup.setName( getName() );
        dup.setSurname( getSurname() );
        return dup;
    }
}
