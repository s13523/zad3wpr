package permissions.domain;

public class Address {

	private String country = "";
	private String city = "";
	private String street = "";
	private String postalCode = "";
	private String houseNumber = "";
	private String localNumber = "";
	
	private int person;

	public String getCountry() {
		return country;
	}

	public Address setCountry(String country) {
		this.country = country;
        return this;
	}

	public String getCity() {
		return city;
	}

	public Address setCity(String city) {
		this.city = city;
        return this;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getLocalNumber() {
		return localNumber;
	}

	public void setLocalNumber(String localNumber) {
		this.localNumber = localNumber;
	}

	public int getPersonId() {
		return person;
	}

	public Address setPersonId(int person) {
		this.person = person;
        return this;
	}

    @Override
    public boolean equals( Object comparee ){
        if( !( comparee instanceof Address ) )
            return false;

        Address address = (Address)comparee;
        return address.getCountry().equals(this.getCountry()) &&
            address.getCity().equals(this.getCity()) &&
            address.getStreet().equals(this.getStreet()) &&
            address.getPostalCode().equals(this.getPostalCode()) &&
            address.getHouseNumber().equals(this.getHouseNumber()) &&
            address.getLocalNumber().equals(this.getLocalNumber());
    }
}
